Contents of This File
---------------------
 * Introduction
 * Usage

Introduction
------------
This module provides a simple way to expose entity properties in the field UI's
"Manage fields" and "Manage display".

Usage
-----
To expose your own entity properties perform the following steps.

Add the extra fields controller class (EntityPropertyExtraFieldsController)
to the entity info that contains the properties that you want to expose.
You can do this just adding a new key in your ```hook_entity_info()```, or you
can implement ```hook_entity_info_alter()```.

	'extra fields controller class' => 'EntityPropertyExtraFieldsController'
  
See [the docs on d.o for more info](https://www.drupal.org/node/2220557).

In ```hook_entity_property_info()``` add an key called 'extra_fields' to each 
element in the property array.  The value should be an array which contains
values of ```form``` and/or ```display```, which indicate that the property
should be available in the edit form and/or the display, respectively.

Alternatively this can be added via ```hook_entity_property_info_alter()```.

Examples
=========
	// Exposes property in create/edit form. 
 	'extra_fields' => array('form'),
  
	// Exposes property as an option in display.
 	'extra_fields' => array('display'),
  
	// Exposes property in both the form and display.
	'extra_fields' => array('form', 'display'),

Complete property example:

	$properties['vid'] = array(
		'label' => t('Revision ID'),
		'type' => 'integer',
		'description' => t('The unique revision identifier.'),
		'schema field' => 'vid',
		'extra_fields' => array('form', 'display'),
	);
